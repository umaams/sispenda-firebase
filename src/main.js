// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vbclass from 'vue-body-class'
import firebase from 'firebase'
import VueFire from 'vuefire'
import VeeValidate from 'vee-validate'
require('popper.js')
window.$ = window.jQuery = require('jquery')
require('bootstrap')
require('bootstrap-notify')
require('./assets/js/main.js')

Vue.use(VeeValidate)
Vue.use(VueFire)
Vue.use(vbclass, router)
Vue.config.productionTip = false

let app
firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    /* eslint-disable no-new */
    /* eslint-env jquery */
    app = new Vue({
      el: '#app',
      router,
      components: {
        App
      },
      template: '<App/>'
    })
  }
})
