import { initializeApp } from 'firebase'

// Initialize Firebase
const app = initializeApp({
  apiKey: 'AIzaSyCQhIvYw-jLwNbL-6qqaZfzd2jMBd4Yn_s',
  authDomain: 'sispenda-b1fd9.firebaseapp.com',
  databaseURL: 'https://sispenda-b1fd9.firebaseio.com',
  projectId: 'sispenda-b1fd9',
  storageBucket: 'sispenda-b1fd9.appspot.com',
  messagingSenderId: '737427322013'
})

export const db = app.database()
export const religionsRef = db.ref('religions')
export const educationsRef = db.ref('educations')
export const occupationsRef = db.ref('occupations')
export const relationsRef = db.ref('relations')
export const subvillagesRef = db.ref('subvillages')
export const populationsRef = db.ref('populations')
export const settingsRef = db.ref('settings')
