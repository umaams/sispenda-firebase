import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Dashboard from '@/components/modules/Dashboard'
import Profile from '@/components/modules/Profile'
import Setting from '@/components/modules/Setting'
import SubvillageView from '@/components/modules/subvillage/View'
import SubvillageCreate from '@/components/modules/subvillage/Create'
import SubvillageEdit from '@/components/modules/subvillage/Edit'
import PopulationView from '@/components/modules/population/View'
import PopulationCreate from '@/components/modules/population/Create'
import PopulationEdit from '@/components/modules/population/Edit'

import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: {
        bodyClass: 'app sidebar-mini rtl',
        requiresAuth: true
      },
      children: [
        {
          path: 'dashboard',
          component: Dashboard,
          meta: {
            title: 'Dashboard',
            description: 'Lihat dashboard aplikasi'
          }
        },
        {
          path: 'profile',
          component: Profile,
          meta: {
            title: 'Profil',
            description: 'Lihat dan ubah profil saya'
          }
        },
        {
          path: 'setting',
          component: Setting,
          meta: {
            title: 'Setting',
            description: 'Lihat dan ubah setting'
          }
        },
        {
          path: 'subvillage',
          component: SubvillageView,
          meta: {
            title: 'Dusun',
            description: 'Lihat data dusun'
          }
        },
        {
          path: 'subvillage/create',
          component: SubvillageCreate,
          meta: {
            title: 'Tambah Dusun',
            description: 'Tambah dusun baru'
          }
        },
        {
          path: 'subvillage/edit/:key',
          component: SubvillageEdit,
          meta: {
            title: 'Edit Dusun',
            description: 'Edit profil dusun'
          }
        },
        {
          path: 'population',
          component: PopulationView,
          meta: {
            title: 'Penduduk',
            description: 'Lihat data penduduk'
          }
        },
        {
          path: 'population/create',
          component: PopulationCreate,
          meta: {
            title: 'Tambah Penduduk',
            description: 'Tambah penduduk baru'
          }
        },
        {
          path: 'population/edit/:key',
          component: PopulationEdit,
          meta: {
            title: 'Edit Penduduk',
            description: 'Edit data diri penduduk'
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        title: 'Login',
        description: 'Login'
      }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      meta: {
        title: 'Register',
        description: 'Register'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title + ' | Sistem Informasi Pendataan Desa'
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('login')
  else if (!requiresAuth && currentUser) next('/dashboard')
  else next()
})

export default router
